import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import fetch from 'cross-fetch';
import ErrorLink from './errorLink';
import { ENDPOINT } from '../../constants';

const link = ApolloLink.from([
  ErrorLink,
  createHttpLink({ uri: ENDPOINT.GRAPHQL, fetch }),
]);

export default new ApolloClient({
  link,
  cache: new InMemoryCache(),
});
