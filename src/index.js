import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles.css';
import { BrowserRouter } from "react-router-dom";
import { ApolloProvider } from 'react-apollo';
import client from './services/apollo/client';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </BrowserRouter>
  , document.getElementById('root'));
serviceWorker.unregister();
