import { compose, withHandlers, withState, mapProps } from 'recompose';
import RegisterEventComponent from '../components/RegisterEventComponent';
import { withRouter } from 'react-router-dom'
import { withFormik } from 'formik';
import gql from 'graphql-tag';
import apolloClient from '../services/apollo/client';

const ADD_EVENT = gql`
  mutation($name: String!,$place: String!, $date: String!, $description: String) {
    addEvent(name: $name, place: $place, date: $date, description: $description) {
      _id
      name
      place
      description
      date
      tickets{
        _id
        type
        amount
      }
      registerDate
    }
  }
`;

const handleBack = ({ history }) => () => {
  history.push('/');
}

const onSave = async (
  values,
  { props: { date, history } }
) => {
  try {
    await apolloClient.mutate({
      mutation: ADD_EVENT,
      variables: { ...values, date },
      fetchPolicy: `no-cache`,
    });
    history.push('/');
  } catch (err) {
    console.log(err);
  }
};

export default compose(
  withRouter,
  withState('date', 'setDate', new Date()),
  withState('event', 'setEvent', {
    name: '',
    place: '',
    description: '',
    date: new Date()
  }),
  mapProps(({ location: { state }, ...props }) => ({
    eventId: state && state.id ? state.id : null,
    ...props,
  })),
  withFormik({
    mapPropsToValues: ({ event: { name, place, date, description } }) => ({
      name,
      place,
      date,
      description
    }),
    handleSubmit: onSave,
    displayName: `onSave`,
  }),
  withHandlers({ handleBack })
)(RegisterEventComponent);