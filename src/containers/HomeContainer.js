import { compose, withHandlers, lifecycle, withState } from 'recompose';
import HomeComponent from '../components/HomeComponent';
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag';
import apolloClient from '../services/apollo/client';

const EVENT_LIST = gql`
  query {
    events{
      _id
      name
      place
      description
      date
      tickets{
        _id
        type
        amount
      }
      registerDate
    }
  }
`;

const getEvents = async () =>
  apolloClient.query({
    query: EVENT_LIST,
    fetchPolicy: `no-cache`,
  });

const handlePlus = ({ history }) => () => {
  history.push('event');
}

const handleEdit = ({ history }) => (id) => {
  history.push(`event/${id}`, { id });
}

export default compose(
  withRouter,
  withState('events', 'setEvents', [{}]),
  withHandlers({ handlePlus, handleEdit }),
  lifecycle({
    async componentDidMount() {
      const { setEvents } = this.props;
      const { data: { events } } = await getEvents();
      setEvents(events);
    }
  })
)(HomeComponent);