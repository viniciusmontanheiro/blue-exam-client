import { compose, withHandlers, lifecycle, withState } from 'recompose';
import TicketComponent from '../components/TicketComponent';
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag';
import apolloClient from '../services/apollo/client';
import { withFormik } from 'formik';
import { TICKET_TYPE, TICKET_TYPE_VALUE } from '../constants';

const ADD_TICKET = gql`
  mutation($eventId: String!, $tickets: [TicketInput]!) {
    addTicket(eventId: $eventId, tickets: $tickets) {
      value
    }
  }
`;

const EVENT = gql`
  query($id: String!) {
    event(id: $id) {
      _id
      name
      place
      description
      date
      tickets{
        _id
        type
        amount
      }
      registerDate
    }
  }
`;

const getEvent = async (id) =>
  apolloClient.query({
    query: EVENT,
    fetchPolicy: `no-cache`,
    variables: {
      id
    }
  });

const handleBack = ({ history }) => () => {
  history.push('/');
}

const parseToTicket = ({ values: { standart, front, vip }, event }) => {
  const tickets = [];
  if (standart != null) {
    const st = event.tickets.find((ev) => TICKET_TYPE[ev.type] === 'standart');
    tickets.push({
      _id: st && st._id ? st._id : '',
      type: TICKET_TYPE_VALUE["standart"],
      amount: standart
    });
  }

  if (front != null) {
    const fr = event.tickets.find((ev) => TICKET_TYPE[ev.type] === 'front');
    tickets.push({
      _id: fr && fr._id ? fr._id : '',
      type: TICKET_TYPE_VALUE["front"], amount: front
    });
  }

  if (vip != null) {
    const vi = event.tickets.find((ev) => TICKET_TYPE[ev.type] === 'vip');
    tickets.push({
      _id: vi && vi._id ? vi._id : '',
      type: TICKET_TYPE_VALUE["vip"],
      amount: vip
    });
  }

  console.log(tickets);
  return tickets;
}

const onSave = async (
  values,
  { props: { event, eventId, history } }
) => {
  try {
    await apolloClient.mutate({
      mutation: ADD_TICKET,
      variables: { eventId, tickets: parseToTicket({ values, event }) },
      fetchPolicy: `no-cache`,
    });
    history.push('/');
  } catch (err) {
    console.log(err);
  }
};

const mapByType = (event, type) => event.tickets.find((ev) => TICKET_TYPE[ev.type] === type)

export default compose(
  withRouter,
  withState('event', 'setEvent', {}),
  withFormik({
    mapPropsToValues: () => ({
      standart: 0,
      front: 0,
      vip: 0,
    }),
    handleSubmit: onSave,
    displayName: `onSave`,
  }),
  withHandlers({ handleBack, onSave }),
  lifecycle({
    async componentDidMount() {
      const { eventId, setFieldValue, setEvent } = this.props;
      const { data: { event } } = await getEvent(eventId);
      setEvent(event);
      const standart = mapByType(event, 'standart') || {};
      setFieldValue('standart', standart.amount);
      const front = mapByType(event, 'front') || {};
      setFieldValue('front', front.amount);
      const vip = mapByType(event, 'vip') || {};
      setFieldValue('vip', vip.amount);
    }
  })
)(TicketComponent);