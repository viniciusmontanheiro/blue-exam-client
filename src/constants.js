export const ENDPOINT = {
  GRAPHQL : 'http://localhost:3000/graphql'
}

export const TICKET_TYPE = Object.freeze({
  0: `standart`,
  1: `front`,
  2: `vip`,
});

export const TICKET_TYPE_VALUE = Object.freeze({
'standart':0,
'front': 1,
'vip': 2
});