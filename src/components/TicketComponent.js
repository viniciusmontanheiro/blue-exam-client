import React from 'react';
import PropTypes from 'prop-types';

const TicketComponent = ({ handleBack, handleSubmit, values, handleChange}) => (
  <div className="col-md-12">
    <div className="table-responsive">
      <table className="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Tipo</th>
            <th>Quantidade</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Standart</td>
            <td><input type="number" min="0" value={values.standart} onChange={handleChange('standart')} /></td>
          </tr>
          <tr>
            <td>Front</td>
            <td><input type="number" min="0" value={values.front} onChange={handleChange('front')} /></td>
          </tr>
          <tr>
            <td>Vip</td>
            <td><input type="number" min="0" value={values.vip} onChange={handleChange('vip')}  /></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div className="col-md-8">
      <div className="col-md-4 pull-right actions">
        <button type="button" onClick={handleBack} className="btn btn-secondary">Voltar</button>
        <button type="button" onClick={handleSubmit} className="btn btn-primary">Salvar</button>
      </div>
</div>
  </div>

);

TicketComponent.propTypes = {
  handleBack: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  event: PropTypes.shape({}).isRequired,
  values: PropTypes.shape({}).isRequired
}

export default TicketComponent;