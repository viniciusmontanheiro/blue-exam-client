import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const RegisterEventFormComponent = ({ handleBack, handleSubmit, handleChange, date, setDate }) => (<div className="container">
  <div className="col-md-12">
    <div className="col-md-6">
      <div className="form-group">
        <input type="text" className="form-control" onChange={handleChange('name')} id="name" placeholder="Nome do evento" required />
      </div>
      <div className="form-group">
        <input type="text" className="form-control" onChange={handleChange('place')} id="place" placeholder="Local do evento" required />
      </div>
    </div>
    <div className="col-md-6">
      <div className="form-group">
        <DatePicker
          selected={date}
          onChange={(selected) => setDate(selected)}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={15}
          dateFormat="MMMM d, yyyy h:mm aa"
          timeCaption="time"
          placeholderText="Selecione a data" />
      </div>
      <div className="form-group">
        <textarea placeholder="Descrição" id="description" rows="4" cols="20" onChange={handleChange('description')} />
      </div>
    </div>
  </div>
  <div className="col-md-8">
    <div className="col-md-4 pull-right actions">
      <button type="button" onClick={handleBack} className="btn btn-secondary">Voltar</button>
      <button type="button" onClick={handleSubmit} className="btn btn-primary">Salvar</button>
    </div>
  </div>
</div>);

RegisterEventFormComponent.propTypes = {
  handleBack: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  date: PropTypes.any.isRequired,
  setDate: PropTypes.func.isRequired
}

export default RegisterEventFormComponent;