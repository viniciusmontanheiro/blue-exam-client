import React from 'react';
import PropTypes from 'prop-types';
import ContentComponent from './ContentComponent';
import RegisterEventFormComponent from './RegisterEventFormComponent';
import TicketContainer from '../containers/TicketContainer';

const RegisterEventComponent = ({ handleBack, handleSubmit, handleChange, date, setDate, event, eventId }) => (<div className="container">
  <ContentComponent title={!!eventId ? `Ingressos` : 'Novo evento'}>
    {!eventId ? (<RegisterEventFormComponent handleBack={handleBack} handleSubmit={handleSubmit}
      handleChange={handleChange} date={date} setDate={setDate} />) 
      : <TicketContainer eventId={eventId}/>}
  </ContentComponent>
</div>);

RegisterEventComponent.propTypes = {
  handleBack: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  date: PropTypes.any.isRequired,
  setDate: PropTypes.func.isRequired,
  eventId: PropTypes.any
}

export default RegisterEventComponent;