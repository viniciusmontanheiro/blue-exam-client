import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const EventCardComponent = ({ event: { name, date, place, description } }) => (
  <div className="card shadow">
    <img className="img-responsive" src="https://blogmedia.evbstatic.com/wp-content/uploads/wpmulti/sites/3/2016/05/10105129/discount-codes-reach-more-people-eventbrite.png" width="100%" height="150" alt={'name'} />
    <div className="card-body">
      {
        name && (<p className="card-text">
          Nome: {name}
        </p>)
      }
      {
        date && (<p className="card-text">
          Data: {moment(date).format("LLL")}
        </p>)
      }
      {
        place && (<p className="card-text">
          Local: {place}
        </p>)
      }
      {
        description && (<p className="card-text">
          Descrição: {description}
        </p>)
      }
    </div>
  </div>);

EventCardComponent.propTypes = {
  event: PropTypes.shape({}).isRequired,
}

export default EventCardComponent;