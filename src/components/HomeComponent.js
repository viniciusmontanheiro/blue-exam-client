import React from 'react';
import PropTypes from 'prop-types';
import ContentComponent from './ContentComponent';
import EventCardComponent from './EventCardComponent';

const HomeComponent = ({ handlePlus, events, handleEdit }) => (<>
  <ContentComponent title={'Eventos'}>
    {
      events.map((event) => (
        <div className="col-md-3" key={event._id}>
          <a id="card" href="javascript:void(0)" onClick={() => handleEdit(event._id)}>
            <EventCardComponent event={event} />
          </a>
        </div>)
      )
    }

  </ContentComponent>
  <footer>
    <button className="btn-plus shadow" onClick={handlePlus} title={'Novo evento'}>
      <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </button>
  </footer>
</>);

HomeComponent.propTypes = {
  handlePlus: PropTypes.func.isRequired,
  events: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleEdit: PropTypes.func.isRequired
}

export default HomeComponent;