import React from 'react';
import PropTypes from 'prop-types';

const ContentComponent = ({ title, children }) => (<div className="container">
  <div className="col-md-12 text-left">
    <br />
    <h1>{title}</h1>
    <hr />
  </div>
  <div className="row">
      {children}
  </div>
</div>);

ContentComponent.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
}

export default ContentComponent;