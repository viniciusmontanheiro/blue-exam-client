import React from "react";
import { Switch, Route } from "react-router-dom";
import HomeContainer from './containers/HomeContainer';
import RegisterEventContainer from './containers/RegisterEventContainer';

const App = () => (
  <main role="main">
    <Switch>
      <Route exact path="/" component={HomeContainer} />
      <Route path="/event" component={RegisterEventContainer} />
      <Route path="/event/:id" component={RegisterEventContainer} />
    </Switch>
  </main>
);

export default App;
